# Program that prints all the numbers from 1 to 100. 
# However, for multiples of 3, instead of the number, print "Linio". 
# For multiples of 5 prin "IT".
# For numbers which are multiples of both 3 and 5, print "Linianos".

# First of all, install the project with composer:
composer install

# To launch the PHP server locally run:
php -S localhost -t public

# To test run:
./vendor/bin/phpunit ./test/LinianosTest.php
