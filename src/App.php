<?php
namespace Linio\Linianos;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class App
{
    private $app;
    public function __construct() {
      $app = new \Slim\App;
      $app->get('/', function (Request $request, Response $response) {

          for($i=1;$i<=100;$i++){
            $a=$i;
            $b=$i;
            $c=$i;
            $array[$i]=$i;
            while($a%3<1 && $a>=3){
              $array[$a]='Linio';
              $a++;
            }
            while($b%5<1 && $b>=5){
              $array[$b]='IT';
              $b++;
            }
            while($c%15<1 && $c>=15){
              $array[$c]='Linianos';
              $c++;
            }
          }

          return implode(' ', $array);
      });
      $this->app = $app;
    }

    public function get(){
        return $this->app;
    }

}
